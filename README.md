# Mini Blog-app with Laravel 9
[See resalt ->](https://gitlab.com/sbaaipook/blog-app/-/tree/main/blog-app-images/that-s-all.png "result")

## create project :

<script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>

<!-- Add a "copy" button next to your code block -->
<button class="btn btn-secondary" data-clipboard-target="#code-block">Copy</button>

<!-- Wrap your code block in a div with a unique ID -->
<div id="code-block">
  <pre>
    <code>
        composrer create-project laravel/laravel blog-app
    </code>
  </pre>
</div>

[See the step in terminal ->](https://gitlab.com/sbaaipook/blog-app/-/tree/main/blog-app-images/create-project.png "create-project")

You can use `laravel new blog-app` it's work as the cmd above:

before run `laravel new blog-app` should run  : `composer global require laravel/installer`


## Using blade template :

<!-- Wrap your code block in a div with a unique ID -->
<div id="code-block">
  <pre>
    <code>
        composrer require laravel/breeze --dev
        php artisan breeze:install blade
        npm run dev
    </code>
  </pre>
</div>
[---->](https://gitlab.com/sbaaipook/images/-/blob/main/blog-app-images/larvel-breeze.png "useing laravel breeze")

## create database blog__app:
Execute request in phpMyAdmin
<div id="code-block">
  <pre>
    <code>
      CREATE DATABASE blog__app;
    </code>
  </pre>
</div>

[create db blog__app --->](https://gitlab.com/sbaaipook/blog-app/-/blob/main/blog-app-images/create-db-blog__app.png "db")

## Migration :

Execute in terminal:
<div id="code-block">
  <pre>
    <code>
      php artisan migrate
    </code>
  </pre>
</div>

[Migrate tables -->](https://gitlab.com/sbaaipook/blog-app/-/blob/main/blog-app-images/migrate-tables.png "migration")

## Create PostController, Post 'Model' , posts 'table':

<div id="code-block">
  <pre>
    <code>
        php artisan make:model -mrc Post 
    </code>
  </pre>
</div>

[Model , Controller, Table -->](https://gitlab.com/sbaaipook/blog-app/-/blob/main/blog-app-images/create-model-migration-controller.png "mrc")

## Routes :

`Route::resource('posts', PostController::class)
  ->only(['create','store'])
  ->middleware(['auth','verifed']);`


[routes -->](https://gitlab.com/sbaaipook/blog-app/-/blob/main/blog-app-images/edit-routes-web-php.png "route")


<!-- Initialize clipboard.js -->
<script>
  new ClipboardJS('.btn');
</script>
