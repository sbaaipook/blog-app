<x-app-layout>
  
  <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
    <h1 class="text-gray-700 text-3xl font-bold my-4">{{ $post->title }}</h1>
    <img class="rounded-md" src="{{ url($post->image) }}" alt="{{ $post->title }}"/>
    <p class=" leading-6 text-gray-600 my-4 text-center">{{ $post->post_content }}</p>
  </div>

</x-app-layout>
