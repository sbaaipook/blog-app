<x-app-layout>
  
 <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
    <h1 class="text-gray-700 font-bold text-2xl my-4">{{ __('All Posts') }}</h1>
    @foreach ($posts as $post) 
    <div class="flex gap-5 w-full p-2 my-4 bg-white h-max rounded-md">
      <div class="flex w-1/3">
        <img class="rounded-tl-md rounded-bl-md object-cover w-full h-full" src="{{ $post->image }}" alt="{{ $post->title }}"/>
      </div>
      <div class="w-2/3 relative">
        <div>
          <h1 class="text-gray-800 text-2xl font-bold mb-2 justify-start">{{ $post->title }}</h1>
          <div>
            <span class="text-gray-600">{{ $post->user->name }}</span>
            <small class="ml-2 text-xs text-gray-600">{{ $post->created_at->format('j M Y, g:i a') }}</small>
            @unless ($post->created_at->eq($post->updated_at))
            <small class="text-sm text-gray-600"> &middot; {{ __('edited') }}</small>
            @endunless
          </div>
        </div>
        <p class="text-gray-700 leading-6 justify-self-center mb-12">
          {{ $post->post_content }}
        </p>
        <a href="{{ route('posts.show', $post) }}" class="absolute bottom-0 right-0 block px-4 py-2 bg-gray-700 text-gray-100 rounded-md self-end justify-self-end hover:bg-gray-800 transition duration-500">Read more</a>
      </div>
       @if ($post->user->is(auth()->user()))
        <x-dropdown>
          <x-slot name="trigger">
           <button>
             <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
               <path d="M6 10a2 2 0 11-4 0 2 2 0 014 0zM12 10a2 2 0 11-4 0 2 2 0 014 0zM16 12a2 2 0 100-4 2 2 0 000 4z" />
             </svg>
           </button>
          </x-slot>
        <x-slot name="content">
        <x-dropdown-link :href="route('posts.edit', $post)">
            {{ __('Edit') }}
         </x-dropdown-link>
        <form method="POST" action="{{ route('posts.destroy', $post) }}">
          @csrf
          @method('delete')
          <x-dropdown-link :href="route('posts.destroy', $post)" onclick="event.preventDefault(); this.closest('form').submit();">
             {{ __('Delete') }}
          </x-dropdown-link>
        </form>
        </x-slot>
        </x-dropdown>
        @endif
    </div>
    @endforeach
 </div>
</x-app-layout>
