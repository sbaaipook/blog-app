<x-app-layout>
  
 <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
    <h1 class="text-gray-700 font-bold text-2xl my-4">{{ __('Add a new post') }}</h1>
    <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
      @csrf
      <input 
        type="text"
        name="title"
        class="p-2 border-gray-300 rounded-md my-4 w-full outline-none focus:border-blue-300 transition duration-300" 
        placeholder="Title"
        value="{{ old('title') }}"
      />
      <x-input-error :messages="$errors->get('title')" class="my-2"/>
      <textarea 
        name="post_content"
        placeholder="What's looking for !.."
        class="h-40 max-w-full w-full border-gray-300 focus:border-blue-300 outline-none rounded-md transition duration-300 focus:border-2 focus:shadow focus:shadow-blue-200"
      >{{ old('post-content') }}</textarea>
      <x-input-error :messages="$errors->get('post-content')" class="mt-2"/>
      <div class="w-full bg-gray-400 flex justify-center items-center h-10 my-4 rounded-md hover:bg-blue-500 transition duration-500">
          <label for="dropzone-file" class="font-semibold cursor-pointer text-gray-100">Click to upload</label>
          <input name="image" id="dropzone-file" type="file" class="hidden" />
      </div>
      <x-input-error :messages="$errors->get('image')" class="my-2"/>
      <button class="text-gray-100 px-4 py-2 bg-blue-500 font-bold rounded-md hover:bg-blue-700 transition duration-400 mt-4" type="submit">Post</button>
  </form>
  </div>
</x-app-layout>
